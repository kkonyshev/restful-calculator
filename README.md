### Description

Simple calculator RESTful service that caches the results of its computations, with the following endpoints:

* ```/add/{a}/{b}```
* ```/add/{a}/{b}/{c}```
* ```/subtract/{a}/{b}```
* ```/subtract/{a}/{b}/{c}```
* ```/multiply/{a}/{b}```
* ```/multiply/{a}/{b}/{c}```
* ```/divide/{a}/{b}/{c}```

# Running application

Next command will run application on ```localhost:8080/calculator```

```sh
$  mvn spring-boot:run
```

Command for run integration tests only without packaging:

```sh
$ mvn clean test
```

### Author

Konstantin Konyshev ```konyshev.konstantin@gmail.com``` [(upwork.com)] [Up]

   [Up]: <https://www.upwork.com/freelancers/~01e7ebce71fba3c1db>
