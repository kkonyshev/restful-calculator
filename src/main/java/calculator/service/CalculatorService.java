package calculator.service;


import calculator.api.CalculationResult;

import java.math.BigDecimal;

/**
 *
 * Created by Konstantin Konyshev on 04/06/16.
 */
public interface CalculatorService {
    CalculationResult add(BigDecimal op1, BigDecimal op2, BigDecimal op3);
    CalculationResult add(BigDecimal op1, BigDecimal op2);

    CalculationResult subtract(BigDecimal op1, BigDecimal op2, BigDecimal op3);
    CalculationResult subtract(BigDecimal op1, BigDecimal op2);

    CalculationResult multiply(BigDecimal op1, BigDecimal op2, BigDecimal op3);
    CalculationResult multiply(BigDecimal op1, BigDecimal op2);

    CalculationResult divide(BigDecimal op1, BigDecimal op2);
}
