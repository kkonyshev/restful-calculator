package calculator.service.impl;

import calculator.api.CalculationResult;
import calculator.service.CalculatorService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 *
 * Created by Konstantin Konyshev on 04/06/16.
 */
@Service
public class CalculatorServiceImpl implements CalculatorService {

    @Cacheable("add")
    @Override
    public CalculationResult add(BigDecimal op1, BigDecimal op2, BigDecimal op3) {
        return add(add(op1, op2).getResult(), op3);
    }

    @Cacheable("add")
    @Override
    public CalculationResult add(BigDecimal op1, BigDecimal op2) {
        return new CalculationResult().setResult(op1.add(op2));
    }

    @Cacheable("subtract")
    @Override
    public CalculationResult subtract(BigDecimal op1, BigDecimal op2, BigDecimal op3) {
        return subtract(subtract(op1, op2).getResult(), op3);
    }

    @Cacheable("subtract")
    @Override
    public CalculationResult subtract(BigDecimal op1, BigDecimal op2) {
        return new CalculationResult().setResult(op1.subtract(op2));
    }

    @Cacheable("multiply")
    @Override
    public CalculationResult multiply(BigDecimal op1, BigDecimal op2, BigDecimal op3) {
        return multiply(multiply(op1, op2).getResult(), op3);
    }

    @Cacheable("multiply")
    @Override
    public CalculationResult multiply(BigDecimal op1, BigDecimal op2) {
        return new CalculationResult().setResult(op1.multiply(op2, MathContext.DECIMAL32));
    }

    @Cacheable("divide")
    @Override
    public CalculationResult divide(BigDecimal op1, BigDecimal op2) {
        return new CalculationResult().setResult(op1.divide(op2, MathContext.DECIMAL32));
    }
}
