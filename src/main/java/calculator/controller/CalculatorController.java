package calculator.controller;

import calculator.api.CalculationResult;
import calculator.service.CalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

/**
 *
 * Created by Konstantin Konyshev on 04/06/16.
 */
@RestController
public class CalculatorController {

    @Autowired
    protected CalculatorService calculatorService;

    @RequestMapping(value = "/add/{op1}/{op2}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> add2(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2)
    {
        return ResponseEntity.ok().body(calculatorService.add(op1, op2));

    }

    @RequestMapping(value = "/add/{op1}/{op2}/{op3}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> add3(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2,
            @PathVariable("op3") BigDecimal op3)
    {
        return ResponseEntity.ok().body(calculatorService.add(op1, op2, op3));

    }


    @RequestMapping(value = "/subtract/{op1}/{op2}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> subtract2(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2)
    {
        return ResponseEntity.ok().body(calculatorService.subtract(op1, op2));

    }

    @RequestMapping(value = "/subtract/{op1}/{op2}/{op3}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> subtract3(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2,
            @PathVariable("op3") BigDecimal op3)
    {
        return ResponseEntity.ok().body(calculatorService.subtract(op1, op2, op3));

    }

    @RequestMapping(value = "/multiply/{op1}/{op2}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> multiply2(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2)
    {
        return ResponseEntity.ok().body(calculatorService.multiply(op1, op2));

    }

    @RequestMapping(value = "/multiply/{op1}/{op2}/{op3}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> multiply3(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2,
            @PathVariable("op3") BigDecimal op3)
    {
        return ResponseEntity.ok().body(calculatorService.multiply(op1, op2, op3));

    }

    @RequestMapping(value = "/divide/{op1}/{op2}", method = RequestMethod.GET, produces=MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<CalculationResult> divide2(
            @PathVariable("op1") BigDecimal op1,
            @PathVariable("op2") BigDecimal op2)
    {
        return ResponseEntity.ok().body(calculatorService.divide(op1, op2));

    }

}
