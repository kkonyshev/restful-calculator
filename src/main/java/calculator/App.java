package calculator;

import calculator.config.CalculatorConfig;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.env.Environment;

import java.io.PrintStream;

/**
 *
 * Created by Konstantin Konyshev on 05/06/16.
 */
@ComponentScan(basePackageClasses = CalculatorConfig.class)
public class App extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(App.class);
        springApplication.setBanner(new CalculatorBanner());
        springApplication.run(args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(App.class);
    }

    /**
     * ASCII art taken from here: http://patorjk.com/software/taag/#p=display&f=Big&t=Calculator
     */
    private static class CalculatorBanner implements Banner {
        @Override
        public void printBanner(Environment environment, Class<?> sourceClass, PrintStream out) {
            out.println("   _____      _            _       _             \n" +
                    "  / ____|    | |          | |     | |            \n" +
                    " | |     __ _| | ___ _   _| | __ _| |_ ___  _ __ \n" +
                    " | |    / _` | |/ __| | | | |/ _` | __/ _ \\| '__|\n" +
                    " | |___| (_| | | (__| |_| | | (_| | || (_) | |   \n" +
                    "  \\_____\\__,_|_|\\___|\\__,_|_|\\__,_|\\__\\___/|_|   \n" +
                    "                                                 \n" +
                    "                                                 ");
        }
    }


}
