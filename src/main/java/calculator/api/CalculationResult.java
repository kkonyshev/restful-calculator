package calculator.api;

import java.math.BigDecimal;

/**
 *
 * Created by Konstantin Konyshev on 04/06/16.
 */
public class CalculationResult {

    private BigDecimal result;

    public BigDecimal getResult() {
        return result;
    }

    public CalculationResult setResult(BigDecimal result) {
        this.result = result;
        return this;
    }
}
