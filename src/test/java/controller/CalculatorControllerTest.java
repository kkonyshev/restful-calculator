package controller;

import calculator.api.CalculationResult;
import calculator.config.CalculatorConfig;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

/**
 *
 * Created by Konstantin Konyshev on 05/06/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {CalculatorConfig.class})
@WebIntegrationTest
public class CalculatorControllerTest {

    @Value("${server.port}")
    private int port = 8080;

    @Value("${server.contextPath}")
    private String contextPath;

    @Value("http://localhost:${server.port}${server.contextPath}")
    private String basePath;

    private RestTemplate rt;

    @Before
    public void init() {
        rt = new RestTemplate();
    }

    @Test
    public void testAdd3() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/add/1/2/3", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(6), result.getResult());
    }

    @Test
    public void testAdd2() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/add/1/2", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(3), result.getResult());
    }

    @Test
    public void testSubtract3() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/subtract/1/2/3", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(-4), result.getResult());
    }

    @Test
    public void testSubtract2() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/subtract/1/2", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(-1), result.getResult());
    }

    @Test
    public void testMultiply3() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/multiply/1/2/3", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(6), result.getResult());
    }

    @Test
    public void testMultiply2() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/multiply/1/2", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(2), result.getResult());
    }

    @Test
    public void testDivide2() throws Exception {
        CalculationResult result = rt.getForObject(basePath + "/divide/1/2", CalculationResult.class);
        Assert.assertEquals(BigDecimal.valueOf(0.5), result.getResult());
    }
}
